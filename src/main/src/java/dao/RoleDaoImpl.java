package web.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import web.model.Role;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
@Transactional
public class RoleDaoImpl implements RoleDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public List<Role> getAllRoles() {
        return entityManager.createQuery("select r from Role r",Role.class).getResultList();
    }

    @Override
    @Transactional(readOnly = true)
    public Role getRoleById(long id) {
        return entityManager.find(Role.class, id);
    }

    @Override
    @Transactional(readOnly = true)
    public Role getRoleByName(String name) {

        return entityManager.createQuery("r from Role r where r.role=:name",Role.class)
                .setParameter("name",name).getSingleResult();
    }

    @Override
    public Set<Role> getRoleSet(String[] names) {
        Set<Role> roleSet = new HashSet<>();
        for(Role role : getAllRoles()){
            for(String name : names){
                if(name.equals(role.getRole())){
                    roleSet.add(role);
                }
            }
        }
        return roleSet;
    }
}
