package web.dao;

import web.model.Role;

import java.util.List;
import java.util.Set;

public interface RoleDao {
    List<Role> getAllRoles();
    Role getRoleById(long id);
    Role getRoleByName(String name);
    Set<Role> getRoleSet(String[] names);
}
