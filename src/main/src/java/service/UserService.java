package web.service;

import web.model.Role;
import web.model.User;

import java.util.List;
import java.util.Set;

public interface UserService {
    List<User> getAllUsers();
    User getUserById(long id);
    void createUser(String name, String surname, int age, String email, String username, String password, String[] roles);
    void editUser(long id, String name, String surname, int age, String email, String username, String password, String[] roles);
    void deleteUser(long id);
}
